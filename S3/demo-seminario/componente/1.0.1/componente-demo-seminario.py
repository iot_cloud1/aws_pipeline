import random
import time
import awsiot.greengrasscoreipc
from awsiot.greengrasscoreipc.model import (
    QOS,
    PublishToIoTCoreRequest
)
import json
from datetime import datetime

TIME_TO_SEND = 5


def convertion_32bit_register2decimal(array):
    dec = (array[0] * 65536 + array[1]) / 1000
    return dec

def send_cloud(measure):
    try:
        print(measure)
        message = json.dumps(measure, indent=4)
        TIMEOUT = 10
        topic = "/demo-seminario/dati"
        qos = 1
        request = PublishToIoTCoreRequest()
        request.topic_name = topic
        request.payload = bytes(message, "utf-8")
        request.qos = qos
        print("## Send to cloud on topic " + topic + " ##")
        print(message)
        ipc_client = awsiot.greengrasscoreipc.connect()
        operation = ipc_client.new_publish_to_iot_core()
        operation.activate(request)
        future_response = operation.get_response()
        future_response.result(TIMEOUT)
    except Exception as e:
        print("Cloud error: " + str(e))


def ModBusReader():
    while(True):
        print("\nModBus Reader Start")
        dict = {}
        dict['Voltage_1'] = convertion_32bit_register2decimal([3, random.randint(30000, 35000)])
        dict['Voltage_2'] = convertion_32bit_register2decimal([3, random.randint(30000, 35000)])
        dict['Voltage_3'] = convertion_32bit_register2decimal([3, random.randint(30000, 35000)])
        dict['hostname'] = 'ec2-t2-micro'
        dict['time'] = datetime.timestamp(datetime.now())
        send_cloud(dict)
        print("\nInvio al cloud")
        time.sleep(TIME_TO_SEND)


ModBusReader()