import boto3
import pandas as pd
import plotly.graph_objects as go

# Inizializza oggetto client per connettersi a AWS Timestream
timestream = boto3.client('timestream-query',
                          region_name='eu-west-1',
                          aws_access_key_id="_______",
                          aws_secret_access_key="_______",
                          )

database_name = "demo-seminario-db"
table_name = "dati"

# Creazione della query
query = f"""
    SELECT *
    FROM "{database_name}"."{table_name}"
    WHERE time BETWEEN ago(1h) AND now()
"""

query_columns = f"""
        SHOW COLUMNS FROM "{database_name}"."{table_name}"
"""

# Esecuzione della query
response = timestream.query(QueryString=query)

# Estrazione dei valori dei sensori
data_points_v1 = []
data_points_v2 = []
data_points_v3 = []
time = []
for row in response['Rows']:
    time.append(row['Data'][2]['ScalarValue'])
    if row['Data'][1]['ScalarValue'] == "Voltage_1":
        data_points_v1.append(float(row['Data'][3]['ScalarValue']))
    elif row['Data'][1]['ScalarValue'] == "Voltage_2":
        data_points_v2.append(float(row['Data'][3]['ScalarValue']))
    elif row['Data'][1]['ScalarValue'] == "Voltage_3":
        data_points_v3.append(float(row['Data'][3]['ScalarValue']))


# Conversione in un dataframe pandas
df_v1 = pd.DataFrame(zip(time, data_points_v1), columns=['time', 'value'])
df_v1['time'] = pd.to_datetime(df_v1['time'])
df_v2 = pd.DataFrame(zip(time, data_points_v2), columns=['time', 'value'])
df_v2['time'] = pd.to_datetime(df_v1['time'])
df_v3 = pd.DataFrame(zip(time, data_points_v3), columns=['time', 'value'])
df_v3['time'] = pd.to_datetime(df_v1['time'])

# Dashboard di visualizzazione
fig = go.Figure()
fig.add_trace(go.Scatter(x=df_v1['time'], y=df_v1['value'], mode='lines', name='Voltage 1'))
fig.add_trace(go.Scatter(x=df_v2['time'], y=df_v2['value'], mode='lines', name='Voltage 2'))
fig.add_trace(go.Scatter(x=df_v3['time'], y=df_v3['value'], mode='lines', name='Voltage 3'))
fig.update_layout(title='Timestream Data Plot',
                  xaxis_title='Time',
                  yaxis_title='Value',
                  template='plotly_dark')
fig.show()
